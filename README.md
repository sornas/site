# LiTHe kod's website

## Requirements

- Rust

## Quickstart

```sh
git clone git@gitlab.com:lithekod/www/site.git
cd site

# Swedish version
cargo run

# English version
SITELANG=en cargo run
```

This will clone the repo, `cd` into it and generate the site in the chosen
language as static HTML into the `public/` directory.

## Adding new pages

Add new markdown files to `website/pages/` representing a Swedish and English
version of the page.

### Redirects

Redirects are handled by `config.yaml`. They are split into `redirects`,
`legacy-redirects` and `legacy-renames`.

"Normal" redirects go in `redirects`. Any new redirects should be added to this list.

`legacy-redirects` create redirects from pages where the language is put in the
suffix, for example `competitions/en -> https://en.lithekod.se/competitions` and
`competitions/se -> https://lithekod.se/competitions`.

`legacy-renames` work like `legacy-redirets` but they also redirect to another
page name. Sort of like a combination between normal redirects and legacy
redirects. For example, `posts -> meetings`,
`posts/en -> https://en.lithekod.se/meetings` and
`posts/se -> https://lithekod.se/meetings`.

**Do not** add a leading `/` to any redirects.

### Sidebar

The sidebar content is also handled by `config.toml`. Simply add a Swedish and English title and what it should link to. **Do not** add a leading `/`.
