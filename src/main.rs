use std::path::{Path, PathBuf};

use color_eyre::eyre::Result;
use fs_extra::dir::CopyOptions;
use serde::Deserialize;
use tera::{Context, Tera};

#[derive(Deserialize)]
struct NavigationItem {
    swedish: String,
    english: String,
    link: String,
}

#[derive(Deserialize)]
struct Redirect {
    old: String,
    new: String,
}

#[derive(Deserialize)]
struct Config {
    basename: String,
    build_dir: PathBuf,
    navigation: Vec<NavigationItem>,
    redirects: Vec<Redirect>,
    #[serde(rename = "legacy-redirects")]
    legacy_redirects: Vec<String>,
    #[serde(rename = "legacy-renames")]
    legacy_renames: Vec<Redirect>,
}

fn parse_page_url(path: &Path) -> Result<String> {
    let path = path.strip_prefix("site/content")?;
    let dir = path.parent().unwrap();
    let stem = path
        .file_stem()
        .and_then(|os| os.to_str())
        .unwrap()
        .trim_end_matches("_en")
        .trim_end_matches("_se");
    Ok(dir.join(stem).to_str().unwrap().to_string())
}

fn setup_context(
    language: &str,
    language_other: &str,
    link_base_other: &str,
    navigation: &[NavigationItem],
) -> Result<(Tera, Context)> {
    let mut tera_ = Tera::new("site/templates/**/*")?;
    let mut context = Context::new();

    // TODO implement
    context.insert("last_updated", "");
    context.insert("selected", &0);

    // TODO remove (unused)
    context.insert("injection", "");

    // constant variables
    context.insert(
        "navigation",
        &navigation
            .iter()
            .map(
                |NavigationItem {
                     swedish,
                     english,
                     link,
                 }| ((swedish, english), link),
            )
            .collect::<Vec<_>>(),
    );

    // variable variables
    context.insert("lang", language);
    context.insert("other_lang", language_other);
    context.insert("other_base", &link_base_other);

    tera_.add_raw_template(
        "page",
        &std::fs::read_to_string("site/templates/page.html")?,
    )?;
    tera_.add_raw_template(
        "redirect",
        &std::fs::read_to_string("site/templates/redirect.html")?,
    )?;
    Ok((tera_, context))
}

fn render_page(source: &Path, build_dir: &Path, tera: &Tera, mut context: Context) -> Result<()> {
    let content = markdown::to_html_with_options(
        &std::fs::read_to_string(source)?,
        &markdown::Options {
            parse: markdown::ParseOptions::gfm(),
            compile: markdown::CompileOptions {
                allow_dangerous_html: true,
                ..markdown::CompileOptions::gfm()
            },
        },
    )
    .unwrap();

    // calculate url from path
    let page_url = parse_page_url(source)?;

    context.insert("html", &content);
    context.insert("page_url", &page_url);

    let rendered = tera.render("page", &context)?;
    let write_path = build_dir.join(page_url).with_extension("html");
    std::fs::create_dir_all(write_path.parent().unwrap())?;
    std::fs::write(&write_path, rendered)?;

    Ok(())
}

fn render_redirect(
    old: &Path,
    redirect_url: &str,
    build_dir: &Path,
    tera: &Tera,
    mut context: Context,
) -> Result<()> {
    context.insert("redirect_url", &redirect_url);

    let rendered = tera.render("redirect", &context)?;
    let write_path = build_dir.join(old).with_extension("html");
    std::fs::create_dir_all(write_path.parent().unwrap())?;
    std::fs::write(&write_path, rendered)?;

    Ok(())
}

fn main() -> Result<()> {
    color_eyre::install().unwrap();

    let Config {
        basename,
        build_dir,
        navigation,
        redirects,
        legacy_redirects,
        legacy_renames,
    } = serde_yaml::from_str(&std::fs::read_to_string("config.yaml")?)?;

    if build_dir.exists() {
        std::fs::remove_dir_all(&build_dir)?;
    }
    std::fs::create_dir_all(&build_dir)?;

    let sitelang = std::env::var("SITELANG").unwrap_or_else(|_| String::from("se"));
    let is_swedish = sitelang == "se";

    let (lang, other_lang) = if is_swedish {
        ("swedish", "english")
    } else {
        ("english", "swedish")
    };
    let swedish_base = format!("https://{basename}/");
    let english_base = format!("https://en.{basename}/");
    let other_lang_base = if is_swedish {
        &english_base
    } else {
        &swedish_base
    };

    let (tera, context) = setup_context(lang, other_lang, other_lang_base, &navigation)?;

    for entry in walkdir::WalkDir::new("site/content/") {
        let entry = entry?;
        let path = entry.path();
        if path.is_dir() {
            continue;
        }
        let stem = path.file_stem().unwrap();
        if !stem.to_str().unwrap().ends_with(&sitelang) {
            continue;
        }
        render_page(path, &build_dir, &tera, context.clone())?;
    }

    for Redirect { old, new } in &redirects {
        let old = PathBuf::from(old);
        render_redirect(&old, &format!("/{new}"), &build_dir, &tera, context.clone())?;
    }

    // /gamejam/se -> {BASENAME}/gamejam
    // /gamejam/en -> en.{BASENAME}/gamejam
    for page in &legacy_redirects {
        let page_path = PathBuf::from(page);

        render_redirect(
            &page_path.join("se"),
            &format!("{swedish_base}{page}"),
            &build_dir,
            &tera,
            context.clone(),
        )?;
        render_redirect(
            &page_path.join("en"),
            &format!("{english_base}{page}"),
            &build_dir,
            &tera,
            context.clone(),
        )?;
    }

    // /posts    -> /meetings
    // /posts/se -> {BASENAME}/meetings
    // /posts/en -> en.{BASENAME}/meetings
    for Redirect { old, new } in &legacy_renames {
        let old = PathBuf::from(old);

        render_redirect(&old, &format!("/{new}"), &build_dir, &tera, context.clone())?;
        render_redirect(
            &old.join("se"),
            &format!("{swedish_base}{new}"),
            &build_dir,
            &tera,
            context.clone(),
        )?;
        render_redirect(
            &old.join("en"),
            &format!("{english_base}{new}"),
            &build_dir,
            &tera,
            context.clone(),
        )?;
    }

    // Create any needed index.html. For example, if we have both aoc.html and
    // aoc/*.html, move aoc.html -> aocCindex.html.

    for entry in walkdir::WalkDir::new(&build_dir) {
        let entry = entry?;
        let path = entry.path();
        // only look at files
        if path.is_dir() {
            continue;
        }
        let path_but_dir = path.parent().unwrap().join(path.file_stem().unwrap());
        if path_but_dir.exists() {
            std::fs::copy(path, path_but_dir.join("index.html"))?;
            std::fs::remove_file(path)?;
        }
    }

    fs_extra::copy_items(
        &["site/static"],
        &build_dir,
        &CopyOptions::default().overwrite(true),
    )?;

    fs_extra::copy_items(
        &std::fs::read_dir("site/other")?.filter_map(|entry| entry.ok()).map(|entry| entry.path()).collect::<Vec<_>>(),
        &build_dir,
        &CopyOptions::default().overwrite(true),
    )?;

    Ok(())
}
